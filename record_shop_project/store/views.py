#from django.shortcuts import render
from django import template
from django.http import HttpResponse
from .models import Album, Artist, Contact, Booking
from django.template import loader
#from .models import ALBUMS
# Create your views here.


def index(request):
	# request albums
	albums = Album.objects.filter(available=True).order_by('-created_at')[:12]
	# then format the request.
	# note that we don't use album['name'] anymore but album.name
	# because it's now an attribute.
	formatted_albums = ["<li>{}</li>".format(album.title) for album in albums]
	#message = """<ul>{}</ul>""".format("\n".join(formatted_albums))
	template = loader.get_template('store/index.html')
	context = {'albums': albums}
	return HttpResponse(template.render(context, request=request))


def listing(request):
	albums = Album.objects.filter(available=True)
	formatted_albums = ["<li>{}</li>".format(album.title) for album in albums]
	message = """<ul>{}</ul>""".format("\n".join(formatted_albums))
	return HttpResponse(message)

def detail(request, album_id):
	album = Album.objects.get(pk=album_id)
	artist = " ".join([artist.name for artist in album.artist.all()])
	message = "The name of the album is {}. It was composed by {}".format(album.title, artist)
	return HttpResponse(message)

def search(request):
	query = request.GET.get('query')
	if not query:
		albums = Album.objects.all()
	else:
		# title contains the query and query is not sensitive to case.
		albums = Album.objects.filter(title__icontains=query)

	if not albums.exists():
		albums = Album.objects.filter(artist__name__icontains=query)

	if not albums.exists():
		message = "Oops we couldn't find any result!"
	else:
		albums = ["<li>{}</li>".format(album.title) for album in albums]
		message = """
			Here are the matching albums:
			<ul>{}</ul>
		""".format("</li><li>".join(albums))

	return HttpResponse(message)