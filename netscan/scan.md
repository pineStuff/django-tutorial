# Installing dependencies:
```
pip3 install python-nmap
pip3 install docxtpl
```

```
{'nmap': {'command_line': 'nmap -oX - -p 400-800 -sV localhost',
          'scaninfo': {'tcp': {'method': 'connect', 'services': '400-800'}},
          'scanstats': {'downhosts': '0',
                        'elapsed': '6.36',
                        'timestr': 'Sun May  9 22:58:31 2021',
                        'totalhosts': '1',
                        'uphosts': '1'}},
 'scan': {'127.0.0.1': {'addresses': {'ipv4': '127.0.0.1'},
                        'hostnames': [{'name': 'localhost', 'type': 'user'},
                                      {'name': 'localhost', 'type': 'PTR'}],
                        'status': {'reason': 'conn-refused', 'state': 'up'},
                        'tcp': {631: {'conf': '10',
                                      'cpe': 'cpe:/a:apple:cups:2.3',
                                      'extrainfo': '',
                                      'name': 'ipp',
                                      'product': 'CUPS',
                                      'reason': 'syn-ack',
                                      'state': 'open',
                                      'version': '2.3'}},
                        'vendor': {}}}}
```