#!/usr/bin/env python3
from docxtpl import DocxTemplate

tpl = DocxTemplate('scan.docx')

context = {
	'col_labels': ['PORT', 'PROTOCOL'],
	'tbl_contents': [
		{'label': '22', 'cols': 'ssh'},
		{'label': '80', 'cols': 'http'},
		{'label': '443', 'cols': 'https'},
	],
}

tpl.render(context)
tpl.save('output_Scan.docx')