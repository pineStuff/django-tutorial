# First chapter

django-admin startproject record_shop_project

pip install psycopg2 # didn't work

pip install psycopg2-binary #worked


installing postgresql:
sudo apt install postgresql

then when running falling into this issue:

```
psql: error: FATAL:  role "user" does not exist
```
so you need to login as posgres user:

```
sudo su - postgres
```

### how to quit postgresql console?
```
\q
```

### Verify the status of the postgresql service:
```
service postgresql status
```

### Creating a new database:
First of all exit psql, createdb has nothing to do with psql command line
```
createdb -O postgres record_shop
```

You still need to be connected as your postgres user.

Also once finished enter the psql command line and verify the database was
created.

```
>psql
postgres=# \l
   Name     |  Owner   | Encoding |
------------+----------+----------+
postgres    | postgres | UTF8     |
record_shop | postgres | UTF8     |
template0   | postgres | UTF8     |
```

### Connecting your django server to the psql database:
Of course let's run into issues!


Of course never follow tutorials, (tutorials are bad).
They advise the following:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'recored_shop',
        'USER': 'user',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '5432',
    }
}
```
The issue with that is as follows:
Your django server will fail, and you only want to user a local unix connection.
If you want to use a local password less connection then you need to remove the
values "HOST", "PORT" and "PASSWORD".

The good way:
In django settings.py:
Set the following values:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'record_shop',
        'USER': 'postgres',
    }
}
```

What else?
From your normal user outside the virtual env, in the file
/etc/postgresql/12/main/pg_hba.conf the following line to this value:
```
# Database administrative login by Unix domain socket
local   all             postgres                                trust
```

Now in order to run your django server run:

```
record_shop_project/manage.py runserver
```

Open your browser and run the following:

```
localhost:8000
```

You should see a congratulation page. (In two words, good job)

Then run the command:
```
record_shop_project/manage.py migrate
```

You should try to run the previous "runserver" and check if it still works.

# Second chapter

In the virtual env

```
pip install django-debug-toolbar
```

In settings.py add to INSTALLED_APPS:

```

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',
]
```

In MIDLEWARES:

```
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]
```
This line MUST be at the end.

Finally add this line:

```
INTERNAL_IPS = ['127.0.0.1']
```

Now add modifications to urls.py

```
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
```

### Creating a new application

```
django-admin startapp store
```

In settings.py add

```
# Application definition

INSTALLED_APPS = [
    'store.apps.StoreConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',
]
```

But it doesn't work for now, it should say,
```
ModuleNotFoundError: No module named 'store'
```

# Third chapter
# Fourth chapter

Create the following directories:

```
mkdir store/templates
mkdir -p store/templates/store
```
And create the following file, that will contain urls for the application
"store".

```
touch store/urls.py
```

# Fifth chapter

We have created a new application, it's template, and now we can access it
through the url domain/store

# Tenth chapter
We have created the migration script with:
```
./manage.py makemigrations
```

Then we run the script, so we migrated to the new database scheme.

Now let's verify that everything works correctly.

Let's login as postgres user:

```
sudo su - postgres
```

Then open the record_shop database:

```
psql record_shop
```

We can enter the following command to list all the tables:

```
\dt
```

In order to check only one table:
```
\d store_contact

record_shop-# \d store_contact
                                    Table "public.store_contact"
 Column |          Type          | Collation | Nullable |                  Default                  
--------+------------------------+-----------+----------+-------------------------------------------
 id     | bigint                 |           | not null | nextval('store_contact_id_seq'::regclass)
 email  | character varying(100) |           | not null | 
 name   | character varying(200) |           | not null | 
Indexes:
    "store_contact_pkey" PRIMARY KEY, btree (id)
Referenced by:
    TABLE "store_booking" CONSTRAINT "store_booking_contact_id_d46d585e_fk_store_contact_id" FOREIGN KEY (contact_id) REFERENCES store_contact(id) DEFERRABLE INITIALLY DEFERRED
```
# Eleventh chapter

How to open Django's console?

```
manage.py shell
>>>
```
How to manually add entries to the database from the console?
In the shell enter the commands:
```
>>> from store.models import Artist, Album
>>> john = Artist(name="John Lennon")
>>> john.save()
>>> john.name = "A small test"
>>> john.save()

# Now creating a album:

>>> album = Album.objects.create(title="Lennon")
>>> album.artists.add(john)

```